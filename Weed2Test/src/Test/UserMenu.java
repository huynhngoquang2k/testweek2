package Test;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class UserMenu {
	Book magicOfBooks = new Book();
	Scanner scanner = new Scanner(System.in);

	week2 week2 = new week2();

	public void Menu(ArrayList<BookClass> arrBook, ArrayList<UserClass> arrUser, String Username) {
		System.out.println("1. Display list Book");
		System.out.println("2. Display your favorite Book ");
		System.out.println("3. Search Book by id ");
		System.out.println("4. Sign out ");
		System.out.println("0. Exit ");
		System.out.print("Enter your choice: ");
		int ch = Integer.parseInt(scanner.next());
		System.out.println();
		switch (ch) {
		case 0:
			System.exit(0);
			break;
		case 1:
			DisplayListBook(arrBook);
			Menu(arrBook, arrUser, Username);
			break;
		case 2:
			DisplayFavoriteBook(arrBook, arrUser, Username);
			Menu(arrBook, arrUser, Username);
			break;
		case 3:
			SearchBookbyID(arrBook, Username, arrUser);
			Menu(arrBook, arrUser, Username);
			break;
		case 4:
			week2.Login(arrUser, arrBook);
			break;
		default:
			System.out.println("Please enter int");
			week2.Login(arrUser, arrBook);
			break;
		}
	}

	public void LogFile(String logMessenger) {
		Logger logger = Logger.getLogger("Log");
		FileHandler fh = null;
		logger.setUseParentHandlers(false);
		try {
			fh = new FileHandler("log.log", true);
			logger.addHandler(fh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);

		logger.info(logMessenger);

	}

	public void DisplayListBook(ArrayList<BookClass> arrBook) {

		for (BookClass book : arrBook) {
			System.out.println(book.toString());
		}
	}

	public void DisplayFavoriteBook(ArrayList<BookClass> arrBook, ArrayList<UserClass> arrUser, String username) {
		for (UserClass user : arrUser) {
			if (user.getUserName().trim().equals(username)) {
				for (int i : user.getFavourite()) {
					for (int j = 0; j < arrBook.size(); j++) {
						if (arrBook.get(j).getBookId() == i) {
							System.out.println(arrBook.get(j).toString());
						}
					}
				}
			}
		}
	}

	public void SearchBookbyID(ArrayList<BookClass> arrBook, String username, ArrayList<UserClass> arrUser) {
		System.out.print("Enter ID_book you want to search: ");
		int id = scanner.nextInt();
		System.out.println();
		magicOfBooks.SearchbyID(arrBook, id);
		System.out.print("Do you want to see detail the book? Enter 'Y' to see detail -> ");
		String tmp = scanner.next();
		if (tmp.trim().toUpperCase().equals("Y")) {
			DisplayBookByID(arrBook, id);
		}
		System.out.print("Do you like it? Enter 'Y' to add book in list favorite book ");
		String tmpString = scanner.next();
		if (tmpString.trim().toUpperCase().equals("Y")) {
			for (UserClass user : arrUser) {
				if (user.getUserName().trim().equals(username)) {
					user.getFavourite().add(id);
					System.out.println("Add favourite successful!");
				}
			}

		}
	}

	public void DisplayBookByID(ArrayList<BookClass> arrBook, int id) {
		Book magic = new Book();
		magic.DisplayBookbyID(arrBook, id);
	}

}

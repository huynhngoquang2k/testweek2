package Test;

import java.util.ArrayList;
import java.util.Scanner;

public class Book {
	Scanner scanner = new Scanner(System.in);

	public void DisplayBook(ArrayList<BookClass> arrBook) {
		for (int i = 0; i < arrBook.size(); i++) {
			System.out.println(arrBook.get(i).toString());
		}
	}

	public void SearchbyID(ArrayList<BookClass> arrBook, int id) {
		for (int i = 0; i < arrBook.size(); i++) {
			if (arrBook.get(i).getBookId() == id) {
				System.out.println(arrBook.get(i).getBookId() + " - " + arrBook.get(i).getBookName());
			}
		}
	}

	public void DisplayBookbyID(ArrayList<BookClass> arrBook, int id) {
		for (BookClass book : arrBook) {
			if (book.getBookId() == id) {
				System.out.println(book.toString());
			}
		}
	}

}

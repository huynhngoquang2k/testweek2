package Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class week2 extends Thread {
	static ArrayList<UserClass> _User;
	static ArrayList<BookClass> _ArrBook;
	static UserMenu user_ = new UserMenu();

	public static void Login(ArrayList<UserClass> user, ArrayList<BookClass> arrBook) {
		Scanner sc = new Scanner(System.in);

		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();

			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName()) && passString.equals(user.get(i).getPassword())) {
					user_.LogFile(userString + " has log in");
					user_.Menu(arrBook, user, userString);
				} else {
					tt = false;

				}
			}
			if (tt == false) {
				user_.LogFile("wrong user or password for user: " + userString);
				System.out.println("404");
			}
		} while (tt == false);
		sc.close();
	}

	public ArrayList<UserClass> ReadFileUser(ArrayList<UserClass> ListUser) {
		String filename = "Data.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					UserClass userAtributes = new UserClass(lineString);
					ListUser.add(userAtributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListUser;
	}

	public ArrayList<BookClass> ReadFileBook(ArrayList<BookClass> ListBook) {
		String filename = "DataBook.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					BookClass bookAttributes = new BookClass(lineString);
					ListBook.add(bookAttributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListBook;
	}

	public static void main(String[] args) throws SecurityException, IOException {
		week2 week2 = new week2();

		ArrayList<BookClass> arrBoookAtributes = new ArrayList<>();
		arrBoookAtributes = week2.ReadFileBook(arrBoookAtributes);
		_ArrBook = arrBoookAtributes;

		user_.LogFile("Start");

		ArrayList<UserClass> arruserAtributes = new ArrayList<>();
		arruserAtributes = week2.ReadFileUser(arruserAtributes);
		_User = arruserAtributes;
		week2.start();

	}

	@Override
	public void run() {
		Login(this._User, this._ArrBook);

	}

}
